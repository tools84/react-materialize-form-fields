# React Materialize Components
Materialize components

## Installation
```

//1. Install Library
npm install gitlab:tools84/react-materialize --save

//or specific tag/release
npm install gitlab:tools84/react-materialize#v1.12 --save



//2. Import additional styles if needed in app.js
import 'my-materialize/lib/main.css';


//3. Load materialize JS and Styles  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
```

!Important:
- dont use 'next/script' to load materialize.min.js



## Components

#### Forms
- Input
- Select
- Checkbox

#### Other
- Action Button
- Breadcrumbs
- Dropdown Menu
- Modal
- Parallax
- Preloader
- Tabs
- Autocomplete Input
- Collapsible
- Date Picker
- Paging
- Tooltip


