import React, { useRef, useEffect } from 'react';


const Datepicker = ({ options, onChange }) => {
	const inputRef = useRef();

	useEffect(() => {
		const _options = onChange ? { ...options, onSelect: onChange } : options;
		const instance = M.Datepicker.init(inputRef.current, _options);
		return () => {
			instance && instance.destroy();
		};
	});

	return <input ref={inputRef} type="text" className="datepicker" />;
};

Datepicker.defaultProps = {
	options: {},
	onChange: false
};
export default Datepicker;
