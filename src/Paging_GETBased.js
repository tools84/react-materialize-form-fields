import React from 'react';



const Paging_GET =(props)=> {
	const {  total, limit , router } =  props;

	const getPagesArr = (startPage, endPage) =>{
		let res = [];
		for (let i = startPage; i <= endPage; i++) {
			res.push(i);
		}
		return res;
	};

	const getStartEnd = (totalPages, currentPage) =>{
		let startPage, endPage;
		currentPage = parseFloat(currentPage);

		// If only few records on list
		if (totalPages <= 10) {
			// less than 10 total pages so show all
			startPage = 1;
			endPage = totalPages;
		} else {
			// Begining of list
			if (currentPage <= 6) {
				// console.log('here 1');
				startPage = 1;
				endPage = 10;
			} else if (currentPage + 5 >= totalPages) {
				// End of list
				startPage = totalPages - 9;
				endPage = totalPages;
			} else {
				//Middle of list
				// console.log('here 3',  { startPage, endPage });
				startPage = currentPage - 5;
				endPage = currentPage + 5;
			}
		}

		return { startPage, endPage };
	};


	const BASE_URL = router.pathname;
	const CURRENT_PAGE = router.query.page || 1;
	const totalPages = total <= limit ? 0 : Math.ceil(Number(total) / limit);

	let { startPage, endPage } =  getStartEnd(totalPages, CURRENT_PAGE);
	let pagesArr =  getPagesArr(startPage, endPage);

	let arrowStyleLast = CURRENT_PAGE == totalPages || CURRENT_PAGE > totalPages - limit ? 'disabled' : 'waves-effect';



	if (totalPages <= 0) {
		return false;
	} else {
		return (
			<ul className="pagination">
				<FirstPageLink CURRENT_PAGE={CURRENT_PAGE} limit={limit} {...props}/>

				{pagesArr.map((page, i) => {
					let istyle = page == CURRENT_PAGE ? 'active' : 'waves-effect';
					const path = getPathFromObj(router.query, page);
					// return (<li key={i} className={istyle}><a href={`${BASE_URL}?page=${page}${router.query.sortBy ? `&sortBy=${router.query.sortBy}`:''}`}>{page}</a></li>);
					return (<li key={i} className={istyle}><a href={`${BASE_URL}?${path}`}>{page}</a></li>);
				})}

				<li className={arrowStyleLast}><a href={`${BASE_URL}?page=${totalPages}${router.query.sortBy ? `&sortBy=${router.query.sortBy}`:''}`}><i className="material-icons">chevron_right</i></a></li></ul>);
	}
};




const getPathFromObj = (pathObj, pageNo)=>{
	let parts=[];

	for(let [key, val] of Object.entries(pathObj)){
		key =='page' ?	parts.push(`${key}=${pageNo}`) : parts.push(`${key}=${val}`) ;
	}

	if(!pathObj['page']) parts.push(`page=${pageNo}`);
	return parts.join('&');
};

const FirstPageLink = ({ router, CURRENT_PAGE, limit })=>{
	const FIRST_PAGE_URL = router.pathname;
	let arrowStyleFirst = CURRENT_PAGE == 1 || CURRENT_PAGE < limit ? 'disabled' : 'waves-effect';

	return(<li className={arrowStyleFirst}>
		<a href={`${FIRST_PAGE_URL}`}><i className="material-icons">chevron_left</i></a>
	</li>)
};


Paging_GET.defaultProps = {
	total: 0,
	limit: 5
};

export default Paging_GET;
