import React from 'react';

export default class Modal extends React.Component {
	static defaultProps = {
		id: 1,
		textTrigger: 'Open Modal'
	};
	constructor(props) {
		super(props);

		this.modalRef = React.createRef();
		this.instance = null;
	}

	componentDidMount() {
		const { onCloseCallback, onOpenCallback } = this.props;

		let options = {
			onCloseStart: () => {
				if (onCloseCallback) onCloseCallback();
			},
			onOpenStart: () => {
				if (onOpenCallback) onOpenCallback();
			}
		};

		this.instance = M.Modal.init(this.modalRef.current, options);
	}

	componentDidUpdate(prevProps, prevState) {
		const { status } = this.props;

		// Prop Status changed
		if (prevProps.status !== status) {
			this.toggleModal(status);
		}
	}

	componentWillUnmount() {
		this.instance.destroy();
	}

	toggleModal = status => {
		status == true ? this.instance.open() : this.instance.close();
	};

	onLinkClick = () => {
		this.toggleModal(true);
	};

	render() {
		const { id, text, modalTrigger, action_component } = this.props;
		const trigger = text ? <a onClick={this.onLinkClick}>{this.props.text}</a> : action_component ? <div onClick={this.onLinkClick}>{action_component}</div> : <a onClick={this.onLinkClick}>Open Modal</a>;

		return (
			<React.Fragment>
				{trigger}

				<div ref={this.modalRef} id={id} className="modal">
					<div className="modal-content">{this.props.children}</div>
				</div>
			</React.Fragment>
		);
	}
}

export const ModalTrigger = ({ children, ModalId }) => {
	return (
		<a className="modal-trigger" href={`#${ModalId}`}>
			{children}
		</a>
	);
};

/*
*  EXAMPLES:
*
*  <ModalTrigger ModalId="ModalName">
*     <i className="material-icons red-text" onClick={e=>this.openDeleteClassModal(item._id, item)} >close</i>
*  </ModalTrigger>
*
*
*  <Modal id="classRecordModal" status={this.state.classRecordModalStatus}>
*     {this.state.ModalFormContent}
*  </Modal>
* */
