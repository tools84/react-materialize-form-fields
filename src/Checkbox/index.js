import React, {useRef, useState, useEffect } from 'react';

/**
 * id - Record/Instance ID
 */
const Checkbox = ({ id, title, checked, disabled, extraClass='', onChange }) => {
	const [_checked, setChecked] = useState(checked);
	const _input = useRef(null);

	useEffect(() => {
		_input.current.checked =  checked;
	}, [checked]);

	const onChangeEvent = id => {
		if (onChange) onChange(id);
	};


	return (
		<label>
			<input ref={_input} type="checkbox" id={id} disabled={!!disabled} className={`${extraClass}`} onChange={() => onChangeEvent(id)} />
			<span>{title}</span>
		</label>
	);
};

Checkbox.defaultProps = {
	id: 1,
	title: '',
	checked: false,
	disabled: false,
	extraClass: false,
	onChange: false
};

export default Checkbox;
