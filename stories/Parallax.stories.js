import React from 'react';
import Parallax from '../src/Parallax';



export default {
    title: 'Other/Parallax',
    component: Parallax,
    argTypes: {},
};

const param={
    image:'http://materializecss.com/images/parallax1.jpg'
};

const Template = (args) => <Parallax {...param} />;
export const Primary = Template.bind({});

