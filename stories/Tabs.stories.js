import React from 'react';
import Tabs from '../src/Tabs';

export default {
	title: 'Other/Tabs',
	component: Tabs,
	argTypes: {}
};

const param = {
	id: 1,
	menuList: ['Tab 1', 'Tab 2']
};

const Template = args => <Tabs {...param} />;
export const Primary = Template.bind({});
