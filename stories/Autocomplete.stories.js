import React from 'react';
import AutocompleteInput from '../src/AutocompleteInput';
import Wrapper from '../.storybook/decorators/Wrapper';


export default {
    title: 'Forms/Autocomplete',
    component: AutocompleteInput,
    argTypes: {},
    // decorators: [story => <Wrapper story={story}/>],
};


const param={
    placeholder:'Insert here',
    data:{
        "Apple": null,
        "Microsoft": null,
        "Google": 'https://placehold.it/250x250'
    }
};
const Template = (args) => <AutocompleteInput  {...param} />;
export const Primary = Template.bind({});

