import React from 'react';
import Collapsible, { CollapsibleItem } from '../src/Collapsible';

export default {
	title: 'Other/CollapsibleItem',
	component: CollapsibleItem,
	argTypes: {}
};

const Template = args => <CollapsibleItem title="Label 1" {...args} />;

// export const Primary = Template.bind({});
