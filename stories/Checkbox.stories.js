import React from 'react';
import Checkbox from '../src/Checkbox';

export default {
	title: 'Forms/Checkbox',
	component: Checkbox,
	argTypes: {}
};

const Template = args => <Checkbox {...args} />;
export const Primary = Template.bind({});
Primary.args = {
	title: 'Checkbox'
};

export const Selected = Template.bind({});
Selected.args = {
	title: 'Checkbox',
	checked: true
};

export const FilledIn = Template.bind({});
FilledIn.args = {
	title: 'Checkbox',
	extraClass: 'filled-in',
	checked: true
};

export const Disabled = Template.bind({});
Disabled.args = {
	title: 'Checkbox',
	checked: true,
	disabled: true
};
