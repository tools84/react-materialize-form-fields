import React from 'react';
import { PreloaderLine, PreloaderSpinner } from '../src/Preloader';



export default {
    title: 'Other/Preloader',
    // component: PreloaderLine,
    argTypes: {},
};

const param={};

const Template = (args) => <PreloaderLine {...param} />;
const Template2 = (args) => <PreloaderSpinner {...param} />;

export const Line = Template.bind({});
export const Spinner= Template2.bind({});

