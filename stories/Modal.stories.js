import React from 'react';
import Modal from '../src/Modal';
// import '../../src/ActionBtn/actionStyles.css';


export default {
    title: 'Other/Modal',
    component: Modal,
    argTypes: {},
};



const Template = (args) => <Modal {...args}>Hello</Modal>;
export const Primary = Template.bind({});

