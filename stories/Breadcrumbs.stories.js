import React from 'react';
import Breadcrumbs from '../src/Breadcrumbs';
// import '../../src/ActionBtn/actionStyles.css';


export default {
    title: 'Other/Breadcrumbs',
    component: Breadcrumbs,
    argTypes: {},
};


const params={
    links:[
        {title:'Store', link:'/'},
        {title:'iPhone', link:'/'},
    ]
};
const Template = (args) => <Breadcrumbs {...params} />;
export const Primary = Template.bind({});

