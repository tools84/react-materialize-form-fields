import React from 'react';
import Tooltip from '../src/Tooltip';

export default {
	title: 'Other/Tooltip',
	component: Tooltip,
	argTypes: {}
};



const Template = args => <Tooltip {...args} />;
export const Primary = Template.bind({});
Primary.args={
	tooltipData: 'Hello',
	icon: 'edit'
};
