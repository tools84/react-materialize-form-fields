import React from 'react';
import Collapsible  from '../src/Collapsible';
import CollapsibleItem  from '../src/CollapsibleItem';

export default {
	title: 'Other/Collapsible',
	component: Collapsible,
	argTypes: {}
};

const params = {};
const Template = args => (
	<Collapsible {...args}>
		<CollapsibleItem title="Title 1">Hello</CollapsibleItem>
		<CollapsibleItem title="Title 2">Hello</CollapsibleItem>
		<CollapsibleItem title="Title 2">Hello</CollapsibleItem>
	</Collapsible>
);
export const Primary = Template.bind({});
Primary.args={}
