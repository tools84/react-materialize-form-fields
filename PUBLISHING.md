#Creating & Publishing the package

Next.js require packages to be build with some requirements:
- no css/scss imports in components
- 'css-loader’ would not work in format ['style-loader', 'css-loader', 'sass-loader’] as css-loader does not support SSR and trying to access document/window


Solution:
- build with webpack
- use MiniCssExtractPlugin to compile all css/scss in one file and remove imports from component level



### Compiling with Webpack
Works well






### Compiling with Babel-cli
```
"build": "babel src --out-dir lib --copy-files"
npm run build

//react-materialize using  babel to build a package
//--out-dir lib //compiled files bundled under lib folder
//--copy-files //imported filed such as css/scss copied as well
```
Doesn't work well for Next.js components as Next.js doesnt allow 
component level style imports.

Babel-cli has no way to compile css/scss imports as a separate file for now.
Solution: create main.scss and @import component styles

 
